const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const path = require('path');

module.exports = {
  mode: 'production',
  optimization: {
    minimizer: [new TerserPlugin()]
  },
  entry: {
    main: [
      './src/main.js',
      'webpack-dev-server/client',
      'webpack/hot/dev-server'
    ]
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new CopyWebpackPlugin({
      patterns: [{ from: 'src/assets', to: 'assets' }]
    })
  ],
  devServer: {
    hot: true,
    port: 8080,
    open: true,
    proxy: {
      '/api': 'http://localhost:3000'
    }
  }
};
