/* eslint-disable no-magic-numbers */
/* eslint-disable no-console */
import { validate } from './email-validator.js';

// eslint-disable-next-line
const createJoinProgramSection = (title, button_title) => {
  const worker = new Worker(new URL('./worker.js', import.meta.url));

  const joinProgramSection = document.createElement('section');
  joinProgramSection.classList.add('join-program');
  joinProgramSection.classList.add('app-section');

  const joinProgramContainer = document.createElement('div');
  joinProgramContainer.classList.add('join-program__container');
  joinProgramSection.appendChild(joinProgramContainer);

  const joinProgramTitle = document.createElement('h2');
  joinProgramTitle.classList.add('join-program__title');
  joinProgramTitle.classList.add('app-title');
  joinProgramTitle.innerText = title;
  joinProgramContainer.appendChild(joinProgramTitle);

  const joinProgramSubTitle = document.createElement('h2');
  joinProgramSubTitle.classList.add('join-program__subtitle');
  joinProgramSubTitle.classList.add('app-subtitle');
  joinProgramSubTitle.innerText =
    'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
  joinProgramContainer.appendChild(joinProgramSubTitle);

  const joinProgramForm = document.createElement('form');
  joinProgramForm.classList.add('join-program__form');
  joinProgramContainer.appendChild(joinProgramForm);

  const emailInput = document.createElement('input');
  emailInput.classList.add('join-program__form__input');
  emailInput.setAttribute('placeholder', 'Email');
  emailInput.setAttribute('type', 'email');
  emailInput.setAttribute('name', 'email');
  emailInput.setAttribute('id', 'email');
  emailInput.setAttribute('required', true);
  joinProgramForm.appendChild(emailInput);

  const submitButton = document.createElement('button');
  submitButton.classList.add('join-program__form__button');
  submitButton.setAttribute('type', 'submit');
  joinProgramForm.appendChild(submitButton);

  const submitButtonSpan = document.createElement('span');
  // eslint-disable-next-line
  submitButtonSpan.innerText = button_title;
  submitButton.appendChild(submitButtonSpan);

  document.addEventListener('DOMContentLoaded', function () {
    const footer = document.querySelector('.app-footer');
    footer.before(joinProgramSection);
    const subscriptionEmail = localStorage.getItem('subscriptionEmail');
    if (subscriptionEmail) {
      emailInput.value = subscriptionEmail;
    }
    const buttonState = localStorage.getItem('buttonState');
    if (buttonState === 'unsubscribe') {
      emailInput.style.display = 'none';
      submitButton.innerHTML = '<span>Unsubscribe</span>';
    }
  });

  /* ----------------------------- HANDLING EMAIL ----------------------------- */
  emailInput.addEventListener('input', function () {
    const email = emailInput.value;
    localStorage.setItem('subscriptionEmail', email);
    worker.postMessage({
      timestamp: Date.now(),
      type: 'clickEvent',
      desc: 'Input has been changed'
    });
  });
  joinProgramForm.addEventListener('submit', function (event) {
    event.preventDefault();
    worker.postMessage({
      timestamp: Date.now(),
      type: 'clickEvent',
      desc: 'Button has been clicked'
    });
    const buttonState = localStorage.getItem('buttonState');
    if (!buttonState) {
      // subscribe
      const email = emailInput.value;
      const isValidEmail = validate(email);
      if (isValidEmail) {
        submitButton.disabled = true;
        // sends ajax post request
        fetch('http://localhost:3000/subscribe', {
          method: 'POST',
          body: JSON.stringify({ email }),
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            if (data.error === 'Email is already in use') {
              submitButton.disabled = false;
              window.alert(data.error);
            } else {
              submitButton.disabled = false;
              localStorage.setItem('subscriptionEmail', email);
              localStorage.setItem('buttonState', 'unsubscribe');
              emailInput.style.display = 'none';
              submitButton.innerHTML = '<span>Unsubscribe</span>';
              console.log('Success:', data);
            }
          })
          .catch((error) => {
            submitButton.disabled = false;
            window.alert('Error:', error);
          });
      }
    } else if (buttonState === 'unsubscribe') {
      // unsubscribe
      submitButton.disabled = true;
      fetch('http://localhost:3000/unsubscribe', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then((response) => response.json())
        .then((data) => {
          submitButton.disabled = false;
          localStorage.removeItem('subscriptionEmail');
          localStorage.removeItem('buttonState');
          emailInput.value = '';
          emailInput.style.display = 'block';
          submitButton.innerHTML = '<span>Subscribe</span>';
          console.log(data);
        })
        .catch((error) => {
          submitButton.disabled = false;
          console.error(error);
        });
    }
  });
};

export { createJoinProgramSection };
