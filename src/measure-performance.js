export default function measureCommunityPage () {
  const startFetchTime = window.performance.now();
  fetch('http://localhost:3000/community')
    .then((response) => response.json())
    .then((data) => {
      const endFetchTime = window.performance.now();
      const fetchTime = endFetchTime - startFetchTime;
      console.log(`Fetching data took ${fetchTime} milliseconds`);

      const pageLoadTime =
        window.performance.timing.loadEventEnd -
        window.performance.timing.navigationStart;
      console.log(`Page load time: ${pageLoadTime} milliseconds`);

      let memoryUsage = 0;
      if (window.performance && window.performance.memory) {
        memoryUsage = window.performance.memory.usedJSHeapSize;
      }
      console.log(`Memory usage: ${memoryUsage} bytes`);

      const metrics = {
        fetchTime,
        pageLoadTime,
        memoryUsage
      };

      fetch('http://localhost:3000/analytics/performance', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(metrics)
      })
        .then((response) => {
          if (response.ok) {
            console.log('Metrics sent successfully');
          } else {
            console.log('Failed to send metrics');
          }
        })
        .catch((error) => {
          console.log('Error sending metrics:', error);
        });
    })
    .catch((error) => {
      console.log('Error fetching data:', error);
    });
}
