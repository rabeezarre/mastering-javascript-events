/* eslint-disable no-magic-numbers */
let clickEvents = [];

self.onmessage = function (e) {
  console.log('clickEvents', clickEvents);
  const data = e.data;

  if (clickEvents.length === 5) {
    fetch('http://localhost:3000/analytics/user', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ clickEvents })
    })
      .then((response) => {
        console.log('Data sent to server', response);
      })
      .catch((error) => {
        console.error('Error sending click event data to server:', error);
      });
    clickEvents = [];
  }

  if (data.type === 'clickEvent') {
    clickEvents.push(data);
  }
};
