class WebsiteSection extends HTMLElement {
  constructor () {
    super();

    const shadow = this.attachShadow({ mode: 'open' });

    const container = document.createElement('div');
    container.setAttribute('class', 'section');

    //  "title"
    const title = document.createElement('h2');
    title.textContent = this.getAttribute('title');

    // "description"
    const description = document.createElement('p');
    description.textContent = this.getAttribute('description');

    // slot element
    const slot = document.createElement('slot');

    container.appendChild(title);
    container.appendChild(description);
    container.appendChild(slot);

    shadow.appendChild(container);
  }
}

customElements.define('website-section', WebsiteSection);
